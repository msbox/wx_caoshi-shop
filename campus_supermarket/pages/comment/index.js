const { addressAdd } = require('../../utils/config');
// pages/comment/index.js
const api = require('../../utils/config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    addressInfo: {userName: '', phone: '', area: '', address: '', code: ''},
    host: api.root_host
    
  },
  addressAction: function(e){
    var address = this.data.addressInfo;
    address.address = e.detail.value;
    this.setData({
      addressInfo: address
    })
  },
  phoneAction: function(e){
    var address = this.data.addressInfo;
    address.phone = e.detail.value;
    this.setData({
      addressInfo: address
    })
  },
  areaAction: function(e){
    var address = this.data.addressInfo;
    address.area = e.detail.value;
    this.setData({
      addressInfo: address
    })
  },
  userNameAction: function(e){
    var address = this.data.addressInfo;
    address.userName = e.detail.value;
    this.setData({
      addressInfo: address
    })
  },
  codeAction: function(e){
    var address = this.data.addressInfo;
    address.code = e.detail.value;
    this.setData({
      addressInfo: address
    })
  },
  ///{id}/comment
  submitAddress: function(){
    var userInfo = wx.getStorageSync('userInfo');
    var addressInfo = this.data.addressInfo;
    var url = '';
    console.log(this.data.id);
    if(this.data.id != undefined){
      url = api.addressItem + this.data.id;
    }else{
      url = api.addressAdd + '/' + userInfo.id;
    }
    wx.request({
      url: url,
      dataType: 'json',
      method: 'post',
      data: {
        "userName": addressInfo.userName,
        "phone": addressInfo.phone,
        "area": addressInfo.area,
        "address": addressInfo.address,
        "code": addressInfo.code
    },
      success: (res) => {
          if(res.statusCode == 200){
            wx.showToast({
              title: "提交成功",
            })
          }
      }
    })
  },
  textareaBInput(e) {
    var product = this.data.product;
    var index = e.currentTarget.dataset.index;
    product[index].comment = e.detail.value;
    this.setData({
      product: product
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id=options.id;
    this.setData({
      id: options.id
    });
    wx.request({
      url: api.addressItem + id ,
      dataType: 'json',
      success: (res) => {
        if(res.statusCode == 200){
          this.setData({
            addressInfo: res.data.data
          })
          console.log(this.data.addressInfo);
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})