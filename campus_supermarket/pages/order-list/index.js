// pages/order-list/index.js
const api = require("../../utils/config");
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: []

  },
  orderAction(status, id){
    app.data.utils.request({
      url: app.data.api.orderStatus + id,
      data: {status: status},
      method: 'post',
      success: (data) => {
        wx.showToast({
          title: '操作成功',
        })
        this.getList();
      },
    })
},
  //支付
  payAction: function(e){
    var id = e.currentTarget.dataset.id;
    this.orderAction(3, id);
  },
  huoAction(e){
    var id = e.currentTarget.dataset.id;
    this.orderAction(99, id);
  },
  cancleAction(e){
    var id = e.currentTarget.dataset.id;
    this.orderAction(98, id);
  },
  //删除订单
  deleteAction: function(e){
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: api.delete_order + '/'+ id,
      dataType:'json',
      method: 'post',
      data: {_method: 'delete'},
      success:(res)=>{
        if(res.statusCode == 200){
          wx.showToast({
            title: '删除成功',
          })
          this.getList();
        }
      }
    })
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  getList: function(){
    var userInfo = wx.getStorageSync('userInfo');
      console.log(userInfo);
      wx.request({
        url: api.orderList + '/'+ userInfo['id'] +'/orders',
        dataType:"json",
        success:(response) =>{
          console.log(response);
            this.setData({
               orderList:  response.data.data
            })
        }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})