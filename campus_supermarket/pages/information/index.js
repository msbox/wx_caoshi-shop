// pages/information/index.js
const api = require('../../utils/config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    real_name: '',
    phone_number: '',


  },
  realName: function(e){
    console.log(e);
      this.setData({
        real_name: e.detail.value,
      })
  },
  phoneNumber: function(e){
    this.setData({
      phone_number: e.detail.value,
    })
  },
  sumit: function(){
    if(!this.data.real_name && !this.data.phone_number){
      wx.showToast({
        title: '请至少填写一项信息',
        icon: 'none'
      })
      return;
    }
    var userInfo = wx.getStorageSync('userInfo');
    wx.request({
      url: api.member + '/' + userInfo.id,
      dataType: 'json',
      method: 'post',
      data: {real_name: this.data.real_name, phone_number: this.data.phone_number},
      success: (res) => {
          if(res.statusCode == 200){
            wx.showToast({
              title: '更新成功',
            })
            wx.setStorageSync('userInfo', res.data.data);
          }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userInfo = wx.getStorageSync('userInfo');
    console.log(userInfo);
    this.setData({
      real_name: userInfo.real_name,
      phone_number: userInfo.phone_number
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})