﻿// pages/order-list/index.js
const api = require("../../utils/config");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: [{address: '重庆市奉节县康乐镇', phone_number: 18883771802, userName: 'ppp', code: '12333'}]

  },
  //编辑收货地址
  editAction: function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/comment/index?id='+id,
    })
  },
  //删除收货地址
  deleteAction: function(e){
    var id = e.currentTarget.dataset.id;
    wx.request({
      url: api.addressAdd + '/'+ id,
      dataType:'json',
      method: 'delete',
      data: {_method: 'delete'},
      success:(res)=>{
        if(res.statusCode == 200){
          wx.showToast({
            title: '删除成功',
          })
          this.getList();
        }
      }
    })
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  getList: function(){
    var userInfo = wx.getStorageSync('userInfo');
      console.log(userInfo);
      wx.request({
        url: api.addressAdd + '/'+ userInfo['id'],
        dataType:"json",
        success:(response) =>{
          console.log(response);
            this.setData({
               orderList:  response.data.data
            })
        }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})