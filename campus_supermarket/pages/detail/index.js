// pages/detail/index.js
const app = getApp();
const api = require('../../utils/config');
const func = require('../../utils/util');
Page({

  /**
   * 页面的初始数据
   */
  data: {},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      wx.request({
        url: api.detail + options.id,
        dataType:"json",
        success:(response) =>{
          console.log(response);
          response.data.data.host = api.root_host;
          this.setData(response.data.data);
        }
      })
  },
  //加入购物车
  addCart: function(e){
    var product = {
      id:this.data.id,
      name:this.data.name,
      image:this.data.image,
      num:'1',
      price:this.data.price,
      sum:this.data.price,
      selected:false
    };
    func.addCart(product);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})