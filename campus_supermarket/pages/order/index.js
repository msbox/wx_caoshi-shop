// pages/order/index.js
const api = require('../../utils/config');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: api.root_host,
    product: [],
    total: 0,
    address_index: null,
    dinner_time: "",
    pay_type: 0, //默认微信支付
    addressInfo: [],
    pay_arr: []
      
  },
  //结算
  orderAction: function(){
    if(!this.data.address_index){
      wx.showToast({
        title: '请选择收货地址',
        icon: "none"
      })
      return;
    }
    //结算请求接口
    var userInfo = wx.getStorageSync('userInfo');
    wx.request({
      url: api.order + '/'+ userInfo.id +'/orders',
      method: 'post',
      dataType: 'json',
      data: {address_id: this.data.addressInfo[this.data.address_index].id, 
        payment_id: this.data.pay_arr[this.data.pay_type].id,
        phone_number: '',
        products: this.data.product
      },
      success: (response)=>{
        if(response.statusCode == 200){
          if(response.data.code != 0){
            wx.showToast({
              title: '订单创建失败',
              icon: 'none'
            })
          }else{
            //清除购物车
            wx.setStorageSync('carts', [])
            wx.showModal({
              title: '支付提示',
              content: '您确定现在支付吗',
              success (res) {
                if (res.confirm) {
                app.data.utils.request({
                    url: app.data.api.orderStatus + response.data.data.id,
                    data: {status: 3},
                    method: 'post',
                    success: (data) => {
                      wx.showLoading({
                        title: '支付成功',
                      })
                      wx.redirectTo({
                        url: '/pages/order-list/index',
                      })
                    }
                  })
                } else if (res.cancel) {
                  wx.showLoading({
                    title: '您取消了支付',
                  })
                  wx.redirectTo({
                    url: '/pages/order-list/index',
                  })
                }
               
              }
            })
        
          }
        }
      }
    })
    
  },
  addressChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      address_index: e.detail.value
    })
  },
  TimeChange: function(e){
    this.setData({
      dinner_time: e.detail.value
    })
  },
  payChange: function(e){
    this.setData({
      pay_type: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   var selectCart = wx.getStorageSync('selectCart');
   console.log(selectCart);
    this.setData({
      product: selectCart.data,
      total: selectCart.total
    })
    var userId = wx.getStorageSync('userInfo')['id'];
    wx.request({
      url: api.addressAdd + '/' + userId,
      dataType: 'json',
      success:(response) => {
        this.setData({
          addressInfo: response.data.data,
        });
        console.log(this.data.addressInfo);
      }
    })
    wx.request({
      url: api.payments,
      dataType: 'json',
      success:(response)=> {
        this.setData({
          pay_arr: response.data.data
        })
      }
    })
    console.log(this.data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})