const app = getApp();
const api = require('../../utils/config');
const util = require('../../utils/util');
Page({
  data: {
      category: [],
      host: api.root_host,
      search_name: ''

  },
  searchName: function(e){
    this.setData({
      search_name: e.detail.value
    })
  },
  findData: function(){
    this.findList(this.data.search_name)
  },
  onShow() {
    this.findList('');
  },
  findList: function(name){
    console.log(111);
    wx.showLoading({
      title: '加载中...',
      mask: true
    });
    wx.request({
      url: api.categories,
      dataType:"json",
      data: {name: name},
      success:(response) =>{
        console.log(response);
          this.setData({
             category:  response.data.data,
          })
      }
    })

    console.log(this.data.category);
    wx.hideLoading()
  },
  //将商品添加到购物车
  addCart: function(e){
    var id = e.currentTarget.dataset.id;
    var category_id = e.currentTarget.dataset.category;
    //获取用户添加的商品全部信息
    var product = {};
    for(var index in this.data.category){
      var item = this.data.category[index];
      if(item.id == category_id){
        if(item.data) for(var idx in item.data){
          if(item.data[idx].id == id){
            var data = item.data[idx];
            product = {
              id:data.id,
              name:data.name,
              image:data.url,
              num:'1',
              price:data.price,
              sum:data.price,
              selected:false
            };
            break;
          }
        }
        break;
      }
    }
    console.log(product);
    util.addCart(product);
   
  },
  
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      MainCur: e.currentTarget.dataset.id,
      VerticalNavTop: (e.currentTarget.dataset.id - 1) * 50
    })
  },
  VerticalMain(e) {
    // let that = this;
    // console.log(e);
    // let list = this.data.list;
    // let tabHeight = 0;
    // if (this.data.load) {
    //   for (let i = 0; i < list.length; i++) {
    //     let view = wx.createSelectorQuery().select("#main-" + list[i].id);
    //     view.fields({
    //       size: true
    //     }, data => {
    //       list[i].top = tabHeight;
    //       tabHeight = tabHeight + data.height;
    //       list[i].bottom = tabHeight;     
    //     }).exec();
    //   }
    //   that.setData({
    //     load: false,
    //     list: list
    //   })
    // }
    // let scrollTop = e.detail.scrollTop + 20;
    // for (let i = 0; i < list.length; i++) {
    //   if (scrollTop > list[i].top && scrollTop < list[i].bottom) {
    //     that.setData({
    //       VerticalNavTop: (list[i].id - 1) * 50,
    //       TabCur: list[i].id
    //     })
    //     return false
    //   }
    // }
  }
})