// pages/index/index.js
const app = getApp();
Page({
  onShareAppMessage: function () {
  },
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    windowWidth: 320,
    sortPanelTop: '0',
    sortPanelDist: '290',
    sortPanelPos: 'relative',
    noticeIdx: 0,
    host: app.data.api.root_host,
    goodsList: [],
    animationNotice: {},
    adList: [],
    TabCur: 0,
    scrollLeft:0
  },
  category: function(){
    wx.switchTab({
      url: '/pages/product/index',
    })
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id-1)*60
    })
  },
  onReady: function() {

  },
  onLoad: function() {
  
  
  },
  onShow: function() {
    //资讯
    app.data.utils.request({
        url: app.data.api.news,
        data: {},
        method: 'get',
        success: (data) => {
          this.setData({
            adList: data,
          })
        }
      })
   
    //产品列表
    app.data.utils.request({
      url: app.data.api.hot,
      data: {},
      method: 'get',
      success: (data) => {
        this.setData({
          goodsList: data,
        })
      }
    })

  },
  getNewsList: function(){
    
  }
  
})