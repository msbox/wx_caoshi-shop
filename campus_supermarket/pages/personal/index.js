// pages/personal/index.js
const app = getApp();
const api = require('../../utils/config');
Page({
  options: {
    addGlobalClass: true,
  },
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    userInfo: wx.getStorageSync('userInfo'),
  },
  onShow: function(e){
    if(wx.getStorageInfoSync('userInfo')){
      var userInfo = wx.getStorageSync('userInfo');
      wx.request({
        url: api.personal_msg + '/' + userInfo.id,
        dataType: 'json',
        success: (res) => {
          wx.setStorageSync('userInfo', res.data.data)
          this.setData({
            userInfo: wx.getStorageSync('userInfo')
          })
        }
      })
    }
  },
  coutNum(e) {
    if (e > 1000 && e < 10000) {
      e = (e / 1000).toFixed(1) + 'k'
    }
    if (e > 10000) {
      e = (e / 10000).toFixed(1) + 'W'
    }
    return e
  },
  bindGetUserInfo (e) {
    var userInfo = e.detail.userInfo;
    console.log(userInfo);
    // 登录
    wx.login({
      success: res => {
        wx.request({
          url: api.login,
          method: 'post',
          data: {code: res.code, nickname: userInfo.nickName, gender: userInfo.gender, avatarUrl: userInfo.avatarUrl},
          dataType: 'json',
          success:(response) => {
            if(response.statusCode == 200){
              console.log(response);
              this.setData({
                userInfo: response.data.data
              })
              //存入缓存
              wx.setStorage({
                data: this.data.userInfo,
                key: 'userInfo',
              })
            }
          }
        })
      }
    })
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  showQrcode() {
    wx.previewImage({
      urls: ['https://image.weilanwl.com/color2.0/zanCode.jpg'],
      current: 'https://image.weilanwl.com/color2.0/zanCode.jpg' // 当前显示图片的http链接      
    })
  },
})