// config.js
/**
 * 小程序后端接口配置文件
 */
var host = "http://127.0.0.1:80/api";
var root_host = "http://127.0.0.1:80";
//var host = "http://valar.pub/api";
//var root_host = "http://valar.pub";
var api = {
  // 下面的地址配合 Server 工作
  host,    
  root_host,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  //配置
  categories: `${host}/categories`,
  //商品详情
  detail: `${host}/products/`,
  //token
  qntoken: `${host}/test/wx/gainqntoken`,
  // 详情
  orderList: `${host}/members`,
  //座位号列表
  seat: `${host}/seats`,
  //支付方式列表
  payments: `${host}/payments`,
  //登录
  login: `${host}/login`,
  //用户下单
  order: `${host}/members`,
  //用户评价列表
  comment_list: `${host}/members`,
  //评论
  comment: `${host}/products`,
  //评论操作
  comment_action: `${host}/members`,
  //完善信息
  member: `${host}/members`,
  //订单商品
  order_product: `${host}/orders`,
  //用户信息
  personal_msg: `${host}/members`,
  //删除评论
  delete_comment: `${host}/comments`,
  //订单删除
  delete_order: `${host}/orders`,
  //行业资讯
  news: `${host}/news`,
  news_detail: `${host}/news`,
  //收货地址
  addressAdd: `${host}/address`,
  hot: `${host}/hot/products`,
  orderStatus: `${host}/orders/status/`,
  addressItem: `${host}/address/detail/`
};
  //对外把对象config返回
module.exports = api;