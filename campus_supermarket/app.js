//app.js
const api = require('./utils/config');
const utils = require('./utils/util');
App({
  data: {
    api: api,
    utils: utils,
    userInfo: {},
  },
  onLaunch: function () {
    console.log(wx.cloud);
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var code = res.code;

      }
    })
    if (wx.cloud) {
      wx.cloud.init({
        traceUser: true
      })
    }
  },
})