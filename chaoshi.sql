/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.26 : Database - chaoshi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `address` */

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `members_id` int(11) NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `address` */

insert  into `address`(`id`,`members_id`,`userName`,`area`,`address`,`code`,`phone`,`created_at`,`updated_at`) values 
(1,1,'12312gfgfdd','重庆3333','地址','12312','18523922701','2021-01-30 22:17:11','2021-02-02 23:42:12'),
(2,1,'ee','fdf','dgfdg','1234','13242342','2021-02-02 23:38:38','2021-02-02 23:38:38'),
(3,1,'dgfd','wretrfed','fdhfgh','343','12121','2021-02-02 23:42:22','2021-02-02 23:42:22'),
(4,2,'平平','重庆市','某某街道','232','34343','2021-11-13 09:55:40','2021-11-13 09:55:40');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'膨化零食','2021-01-30 21:46:55','2021-11-13 08:18:15'),
(2,'生鲜水果','2021-01-30 21:47:01','2021-11-13 08:18:26'),
(3,'生活用具','2021-02-02 23:14:37','2021-11-13 08:18:35'),
(4,'学习用具','2021-02-02 23:44:28','2021-11-13 08:18:43');

/*Table structure for table `configs` */

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `configs` */

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `members` */

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` tinyint(4) NOT NULL DEFAULT '1',
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `points` int(11) NOT NULL DEFAULT '0',
  `openid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `members` */

insert  into `members`(`id`,`nickname`,`real_name`,`phone_number`,`sex`,`pic`,`points`,`openid`,`created_at`,`updated_at`) values 
(2,'浮生.若梦','','',0,'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKib4JZichQok3UaibRHqkknbJ3QxmnY2LCquPPAk1YVPDRKQFot5dO4PVpGUFqIoeuLB7VI6oHu5XzA/132',2,'oCzls5AUFo6ResKurx0PxAZteTBY','2021-11-13 09:52:41','2021-11-13 09:56:08');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2020_12_13_004919_create_orders_table',1),
(5,'2020_12_13_004956_create_seats_table',1),
(6,'2020_12_13_005115_create_categories_table',1),
(7,'2020_12_13_005648_create_products_table',1),
(8,'2020_12_13_005704_create_members_table',1),
(9,'2020_12_13_005744_create_products_comments_table',1),
(10,'2020_12_13_005837_create_orders_products_table',1),
(11,'2020_12_13_012155_create_config_table',1),
(12,'2020_12_20_152656_create_news_table',1),
(13,'2021_01_30_211132_create_address_table',1);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `news` */

insert  into `news`(`id`,`title`,`content`,`created_at`,`updated_at`) values 
(1,'校园超市小程序上线啦','<p>校园超市小程序上线啦，这里由你想买的</p>','2021-01-30 21:50:08','2021-11-13 09:47:57');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `payment_method` tinyint(4) NOT NULL DEFAULT '1',
  `members_id` int(11) NOT NULL,
  `userName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`order_number`,`total_price`,`status`,`payment_method`,`members_id`,`userName`,`area`,`address`,`code`,`phone`,`created_at`,`updated_at`) values 
(1,'NTG6Bk2cqgX8Tp4s',123.00,98,1,1,'12312','重庆','地址','12312','18523722890','2021-02-02 21:42:23','2021-02-02 22:03:50'),
(2,'pu4dT9OUjaR1JxpK',123.00,99,1,1,'12312','重庆','地址','12312','18523722890','2021-02-02 21:43:25','2021-02-02 22:26:19'),
(3,'T4gn7fy9eNf2Htuk',123.00,99,1,1,'12312','重庆','地址','12312','18523722890','2021-02-02 21:47:55','2021-02-02 23:44:53'),
(4,'YibyCvOH24ZfOJPA',123.00,99,1,1,'12312','重庆','地址','12312','18523722890','2021-02-02 21:48:56','2021-02-02 22:34:48'),
(5,'FoJZhmilpJflItaO',123.00,99,1,1,'12312','重庆','地址','12312','18523722890','2021-02-02 21:52:52','2021-02-02 22:27:28'),
(6,'zYqAk3qgZRHgnAA0',135.00,99,1,1,'12312','重庆3333','地址','12312','18523922701','2021-02-02 22:27:57','2021-02-02 22:35:45'),
(7,'kJu96XR6Gzq5P2O2',135.00,98,1,1,'12312','重庆3333','地址','12312','18523922701','2021-02-02 22:31:32','2021-02-02 22:31:42'),
(8,'yzIa47DWPJqJmc7H',181.00,99,1,1,'ee','fdf','dgfdg','1234','13242342','2021-02-02 23:40:19','2021-02-02 23:41:59'),
(9,'0jtu6vkpH6rplp0P',123.00,4,1,1,'12312gfgfdd','重庆3333','地址','12312','18523922701','2021-02-02 23:43:26','2021-02-02 23:45:00'),
(10,'MLnCBpfXAUwZo9TD',37.00,3,1,2,'平平','重庆市','某某街道','232','34343','2021-11-13 09:56:08','2021-11-13 09:56:09');

/*Table structure for table `orders_products` */

DROP TABLE IF EXISTS `orders_products`;

CREATE TABLE `orders_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orders_products` */

insert  into `orders_products`(`id`,`orders_id`,`products_id`,`number`,`price`,`created_at`,`updated_at`) values 
(1,1,3,3,123.00,'2021-02-02 21:42:23','2021-02-02 21:42:23'),
(2,2,3,3,123.00,'2021-02-02 21:43:25','2021-02-02 21:43:25'),
(3,3,3,1,123.00,'2021-02-02 21:47:55','2021-02-02 21:47:55'),
(4,4,3,1,123.00,'2021-02-02 21:48:56','2021-02-02 21:48:56'),
(5,5,3,1,123.00,'2021-02-02 21:52:52','2021-02-02 21:52:52'),
(6,6,3,1,123.00,'2021-02-02 22:27:57','2021-02-02 22:27:57'),
(7,6,4,1,12.00,'2021-02-02 22:27:57','2021-02-02 22:27:57'),
(8,7,3,1,123.00,'2021-02-02 22:31:32','2021-02-02 22:31:32'),
(9,7,4,1,12.00,'2021-02-02 22:31:32','2021-02-02 22:31:32'),
(10,8,3,1,123.00,'2021-02-02 23:40:19','2021-02-02 23:40:19'),
(11,8,6,1,23.00,'2021-02-02 23:40:19','2021-02-02 23:40:19'),
(12,8,5,1,23.00,'2021-02-02 23:40:19','2021-02-02 23:40:19'),
(13,8,8,1,12.00,'2021-02-02 23:40:19','2021-02-02 23:40:19'),
(14,9,3,1,123.00,'2021-02-02 23:43:26','2021-02-02 23:43:26'),
(15,10,3,1,12.00,'2021-11-13 09:56:08','2021-11-13 09:56:08'),
(16,10,7,1,1.00,'2021-11-13 09:56:08','2021-11-13 09:56:08'),
(17,10,8,1,12.00,'2021-11-13 09:56:08','2021-11-13 09:56:08'),
(18,10,9,1,12.00,'2021-11-13 09:56:08','2021-11-13 09:56:08');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(8,2) DEFAULT NULL,
  `pic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tag` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`title`,`description`,`price`,`pic`,`categories_id`,`created_at`,`updated_at`,`tag`) values 
(3,'小王子薯片独立包装办公室休闲零食网红零食膨化食品','<p><span>小王子薯片独立包装办公室休闲零食网红零食膨化食品</span></p>',12.00,'/images/hqAqurFmRntKRekl.webp',1,'2021-01-30 22:06:30','2021-11-13 08:26:02','学生最爱'),
(4,'脆休闲膨化食品小吃零食组合儿童零食大礼包 pk 500g...','<p><span>脆休闲膨化食品小吃零食组合儿童零食大礼包 pk 500g...</span></p>',12.00,'/images/VEBnO2FLzhQTHc3E.jpg',1,'2021-02-02 20:53:39','2021-11-13 08:26:59','学生最爱'),
(5,'新鲜富士山红苹果','<p>新鲜富士山红苹果</p>',9.00,'/images/BK9GUrQkRU3f9Yfq.jpg',2,'2021-02-02 23:11:47','2021-11-13 08:28:29','学生最爱'),
(6,'【顺丰包邮】安徽新鲜草莓 3斤/盒 生鲜水果 真空保鲜 ...','<p><span>【顺丰包邮】安徽新鲜草莓 3斤/盒 生鲜水果 真空保鲜 ...</span></p>',23.00,'/images/28wXTfApXUAzxQiR.jpg',2,'2021-02-02 23:12:39','2021-11-13 08:29:17','学生最爱'),
(7,'两元店东西家居日用品百货店2元店1毛钱的小商品一块钱生活居家','<p><span>两元店东西家居日用品百货店2元店1毛钱的小商品一块钱生活居家</span></p>',1.00,'/images/CwZB7xOfMhnFllda.jpg',3,'2021-02-02 23:13:05','2021-11-13 08:31:07','生活必备'),
(8,'【吸水拖把产品建模与渲染】|工业/产品|生活用品','<p><span>【吸水拖把产品建模与渲染】|工业/产品|生活用品</span></p>',12.00,'/images/zIXWckkJLvda35xx.jpg',3,'2021-02-02 23:14:22','2021-11-13 08:31:58','生活必备'),
(9,'【精选】【50支铅笔套装 橡皮擦 手摇削笔机转笔刀】儿童...','<p><span>【精选】【50支铅笔套装 橡皮擦 手摇削笔机转笔刀】儿童...</span></p>',12.00,'/images/c9ePFHsBKLreDbEM.webp',4,'2021-02-02 23:44:06','2021-11-13 09:46:46','生活必备');

/*Table structure for table `products_comments` */

DROP TABLE IF EXISTS `products_comments`;

CREATE TABLE `products_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `products_id` int(11) NOT NULL,
  `members_id` int(11) NOT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products_comments` */

/*Table structure for table `seats` */

DROP TABLE IF EXISTS `seats`;

CREATE TABLE `seats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seat_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seats` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'admin','admin@qq.com',NULL,'$2y$10$oH73LjDtL.BnVLVfeEL2IO.Q0JiwOKb9odgzCNs//1.Kl3cC/UZze',NULL,'2021-01-30 21:46:41','2021-01-30 21:46:41'),
(2,'admin1','145052dwsd@qq.com',NULL,'$2y$10$.89pYFTxTQNA2mWjBkcmZuLYLe6jb5X6hDvRevvxqDzXs0B.UPbCm',NULL,'2021-02-02 23:15:43','2021-02-02 23:15:43'),
(3,'admin2','2434343',NULL,'$2y$10$9olV528rzmBf9vCP9bAMT.tjMHsIMk10/Y6zdboLQBwG791/hnVNe',NULL,'2021-02-02 23:45:18','2021-02-02 23:45:18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
