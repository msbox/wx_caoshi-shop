<a title="删除" href="javascript:;" onclick="del(this,'{{route($route, [$id])}}')"
   style="text-decoration:none">
    <i class="layui-icon">&#xe640;</i>
</a>

@push('scripts')
    <script>
        /*删除*/
        function del(obj, url) {
            layer.confirm('确认要删除吗？',  (index) => {
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'html',
                    data:{_method:"DELETE"}
                }).done((result) => {
                    //发异步，把数据提交给php
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!', {icon: 1, time: 1000});
                }).error(function () {
                    console.log("error");
                }).always(function () {
                });
                //发异步删除数据
            });
        }
    </script>
@endpush
