<ul class="layui-nav layui-nav-tree site-demo-nav" lay-filter="side">
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe62e;</i><cite>商品管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
            <dd class="">
                <a href="javascript:;" _href="{{route('products')}}">
                    <cite>商品列表</cite>
                </a>
            </dd>
            </dd>
            <dd class="">
            <dd class="">
                <a href="javascript:;" _href="{{route('categories')}}">
                    <cite>商品分类</cite>
                </a>
            </dd>
            </dd>
        </dl>
    </li>
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe634;</i><cite>广告轮播管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
            <dd class="">
                <a href="javascript:;" _href="{{route('news')}}">
                    <cite>广告轮播列表</cite>
                </a>
            </dd>
            </dd>
        </dl>
    </li>
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe62d;</i><cite>地址管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
            <dd class="">
                <a href="javascript:;" _href="{{route('address')}}">
                    <cite>地址管理</cite>
                </a>
            </dd>
            </dd>
        </dl>
    </li>
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe63c;</i><cite>订单管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
            <dd class="">
                <a href="javascript:;" _href="{{route('orders')}}">
                    <cite>订单列表</cite>
                </a>
            </dd>
            </dd>
        </dl>
    </li>
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe612;</i><cite>用户管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
                <a href="javascript:;" _href="{{route('members')}}">
                    <cite>用户列表</cite>
                </a>
            </dd>
        </dl>
    </li>
    <li class="layui-nav-item">
        <a class="javascript:;" href="javascript:;">
            <i class="layui-icon" style="top: 3px;">&#xe631;</i><cite>系统管理</cite>
        </a>
        <dl class="layui-nav-child">
            <dd class="">
                <a href="javascript:;" _href="{{route('users')}}">
                    <cite>系统用户列表</cite>
                </a>
            </dd>
        </dl>
    </li>
</ul>
