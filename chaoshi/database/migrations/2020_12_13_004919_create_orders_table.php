<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number', 32); //订单号
            $table->decimal('total_price')->default(0); // 总金额
            $table->timestamp('time'); // 开始时间
            $table->string('seat_no'); // 座位号
            $table->tinyInteger('status')->default(1); // 1. 未支付 2. 已支付 3. 已取消 4. 已完成
            $table->tinyInteger('payment_method')->default(1); // 支付方式
            $table->integer('members_id'); // 支付方式

            // 地址信息
            $table->string('userName'); //收件人
            $table->string('area'); //地区
            $table->string('address'); //详细地址
            $table->string('code'); //邮编
            $table->string('phone'); //手机号码
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
