<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname')->nullable(); // 昵称
            $table->string('real_name')->nullable(); // 真实姓名
            $table->string('phone_number')->nullable(); // 电话号码
            $table->tinyInteger('sex')->default(1); // 性别
            $table->string('pic')->default(''); // 头像
            $table->integer('points')->default(0); // 积分
            //
            $table->string('openid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
