<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title'); // 标题
            $table->string('description')->default(''); // 详情
            $table->integer('products_id');
            $table->integer('members_id');
            $table->string('nickname')->nullable(); // 管理
            $table->string('product_name')->nullable(); // 产品名称
            $table->text('reply')->nullable(); // 回复
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_comments');
    }
}
