<?php

namespace App;

class Configs extends Entity
{
    const columns = [
        'id' => 'ID',
        'key' => '分类名',
        'value' => '分类名',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    protected $fillable = [
        'key', 'value'
    ];
}
