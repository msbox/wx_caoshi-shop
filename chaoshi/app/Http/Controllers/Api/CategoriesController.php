<?php

namespace App\Http\Controllers\Api;

use App\Categories;
use App\Products;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $name = $request->input('name');
        $query =  Products::query();
        //先将商品搜索出来，然后按照category分类
        if ($name){
            $query = $query->where('title','LIKE', "%{$name}%");
        }
        $products = $query->get()->groupBy('categories_id')->toArray();
        $data = [];
        if (!empty($products)) foreach ($products as $cate_id => $items){
            $item_data = [];
            if(!empty($items)) foreach ($items as $item){
                $item_data[] = [
                    'id' =>$item['id'],
                    'name' => $item['title'],
                    'content' => $item['description'],
                    'price' => $item['price'],
                    'url' => $item['pic']
                ];
            }
            $category = Categories::query()->find($cate_id);
            $data[] = [
                'id' => $category->id,
                'name' => $category->name,
                'data' => $item_data,
            ];
        }


        return $this->ok($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
