<?php

namespace App\Http\Controllers\Api;

use App\Members;
use App\Orders;
use App\Products;
use App\ProductsComments;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getProductsByTag()
    {
        $productMap = Products::query()->get()->groupBy('tag')->toArray();
        $productsData = [];
        foreach ($productMap as $tag => $products) {
            if ($tag == "") {
                continue;
            }
            $productItem['name'] = $tag;
            $productItem['data'] = collect($products)->map(function ($product) {
                return [
                    'id' => $product['id'],
                    'name' => $product['title'],
                    'content' => $product['description'],
                    'price' => $product['price'],
                    'url' => $product['pic']
                ];
            });
            $productsData[] = $productItem;
        }

        return $this->ok($productsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         *   return [
         * //       'id',
         * //       'name',
         * //       'content',
         * //       'sale',
         * //       'price',
         * //       'image',
         * //       'comment'=> [
         * //           [
         * //               'avatarUrl',
         * //               'nickname',
         * //               'content',
         * //               'reply',
         * //               'time'
         * //           ]
         * //       ]
         * //   ];
         */
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::query()->find($id);

        return $this->ok([
            'id' => $product->id,
            'name' => $product->title,
            'content' => $product->description,
            'tag' => $product->tag,
            'price' => $product->price,
            'image' => $product->pic,
            'comment' => $product->comments()->map(function (ProductsComments $comment) {
                return [
                    'avatarUrl' => $comment->member()->pic,
                    'nickname' => $comment->member_nickname,
                    'content' => $comment->description,
                    'reply' => $comment->reply,
                    'time' => $comment->created_at,
                ];
            })
        ]);
    }

    public function postComment(Request $request, $id)
    {
        $comments = $request->input('comment');
        $orderId = $request->input('order_id');
        foreach ($comments as $comment) {
            $product = Products::query()->find($comment['id']);
            $member = Members::query()->find($id);
            ProductsComments::query()->create([
                'title' => '',
                'description' => $comment['comment'],
                'products_id' => $comment['id'],
                'members_id' => $id,
                'product_name' => $product->title,
                'nickname' => $member->nickname,
                'reply' => ''
            ]);
        }
        $order = Orders::query()->find($orderId);
        $order->update([
           'status' => Orders::STATUS_COMMENT
        ]);

        Members::query()->find($order->members_id)->incPoints(2);

        return $this->ok([], '评论成功！积分+2');
    }
}
