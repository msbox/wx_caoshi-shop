<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.orders.list', $this->search($request, Orders::class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {

        $order = Orders::query()->find($id);
        $filed = [];
        switch ($order->status) {
            case Orders::STATUS_PAY:
                $filed = [
                    [
                        'name' => 'status',
                        'type' => 'select',
                        'description' => '状态',
                        'options' => [
                            [
                                'id' => Orders::STATUS_WAIT_FA,
                                'name' => '支付'
                            ]
                        ]
                    ]
                ];
                break;
            case Orders::STATUS_WAIT_FA:
                $filed = [
                    [
                        'name' => 'status',
                        'type' => 'select',
                        'description' => '状态',
                        'options' => [
                            [
                                'id' => Orders::STATUS_WAIT_SH,
                                'name' => '发货'
                            ]
                        ]
                    ]
                ];
                break;
            case Orders::STATUS_WAIT_SH:
                $filed = [
                    [
                        'name' => 'status',
                        'type' => 'select',
                        'description' => '状态',
                        'options' => [
                            [
                                'id' => Orders::STATUS_FINISH,
                                'name' => '完成'
                            ]
                        ]
                    ]
                ];
        }

        return view('admin.orders.edit', ['fields' => $filed,'data' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        Orders::query()->find($id)
            ->update($request->except('id'));

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Orders::query()->find($id)->delete();

        return back()->withInput();
    }

    public function wancheng($id)
    {
        Orders::query()->find($id)->update([
            'status' => Orders::STATUS_FINISH
        ]);

        return back()->withInput();
    }
}
