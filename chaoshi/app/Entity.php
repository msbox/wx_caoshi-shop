<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    const columns = [];

    const searchFields = [];

    const createFields = [];

    const editFields = [];

    /**
     * 是否启用删除动作
     * @var bool
     */
    protected $isDelete = true;

    /**
     * 附加操作
     * @var array
     */
    protected $actions = [];

    public function getIsDeleteAttribute()
    {
        return $this->isDelete;
    }

    public function getExtensionActionsAttribute()
    {
        return $this->actions;
    }

    public function getExtensionFields($extensionKey)
    {
        return $this->actions[$extensionKey]['fields'] ?? [];
    }

}
