<?php

namespace App;

class Orders extends Entity
{
    protected $fillable = [
        'order_number', 'total_price', 'phone', 'status', 'payment_method', 'members_id',
        'userName',
        'area',
        'address',
        'code' ,
        'phone',
    ];

    protected $isDelete = true;

    /**
     *      $table->string('userName'); //收件人
    $table->string('area'); //地区
    $table->string('address'); //详细地址
    $table->string('code'); //邮编
    $table->string('phone'); //手机号码
     */
    const columns = [
        'id' => 'ID',
        'order_number' => '订单号',
        'member_nickname' => '用户',
        'total_price' => '订单金额',
        'status_name' => '订单状态',
        'payment_method_name' => '支付方式',

        'userName' => '收件人',
        'area' => '地区',
        'address' => '详细地址',
        'code' => '邮编',
        'phone' => '手机号码',

        'created_at' => '创建时间',
        'updated_at' => '更新时间',
    ];

    // 搜索字段
    const searchFields = [
        [
            'name' => 'order_number',
            'type' => 'like',
            'description' => '订单号'
        ]
    ];

    const createFields = [];

    const editFields = [
        [
            'name' => 'status',
            'type' => 'select',
            'description' => '状态',
            'options' => [
                [
                    'id' => self::STATUS_FINISH,
                    'name' => '完成'
                ]
            ]
        ]
    ];

    const STATUS_WAIT_PAY = 1;
    const STATUS_PAY = 2;
    const STATUS_WAIT_FA = 3;
    const STATUS_WAIT_SH = 4;
    const STATUS_FINISH = 99;
    const STATUS_COMMENT = 100;

    const status_map = [
        1 => '未支付',
        2 => '已支付',
        3 => '待发货',
        4 => '待收货',
        99 => '完成',
        98 => '已取消',
    ];

    const payment_method_map = [
        1 => '微信支付',
        2 => '支付宝支付'
    ];

    public function member()
    {
        return $this->belongsTo(Members::class, 'members_id')->first();
    }

    public function getMemberNickNameAttribute()
    {
        $member = $this->member();
        return $member ? $member->nickname : "";
    }

    public function getStatusNameAttribute()
    {
        return self::status_map[$this->status] ?? '未知状态';
    }

    public function getPaymentMethodNameAttribute()
    {
        return self::payment_method_map[$this->payment_method] ?? '未知方式';
    }
}
