<?php

namespace App;

class Products extends Entity
{
    //
    protected $fillable = [
        'title', 'description', 'price', 'pic', 'categories_id','tag'
    ];

    // 是否删除
    protected $isDelete = true;

    const columns = [
        'id' => 'ID',
        'category_name' => '类别',
        'title' => '名称',
        'tag' => '标签',
        'pic' => [
            'name' => '图片',
            'type' => 'image',
        ],
        'description' => '描述',
        'price' => '价格',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    const searchFields = [
//        ['name' => 'created_at', 'description' => '创建时间', 'type' => 'date', 'value' => ['', '']],
        ['name' => 'title', 'description' => '商品名称', 'type' => 'like', 'value' => '']
    ];

    const createFields = self::editFields;

    const editFields = [
        [
            'name' => 'title',
            'description' => '名称',
            'type' => 'string',
            'value' => '',
        ],
        [
            'name' => 'categories_id',
            'description' => '分类',
            'type' => 'categories',
            'value' => '',
        ],
        [
            'name' => 'tag',
            'description' => '标签',
            'type' => 'select',
            'options' => [
                [
                    'id' => '学生最爱',
                    'name' => '学生最爱'
                ],
                [
                    'id' => '健康美味',
                    'name' => '健康美味'
                ],
                [
                    'id' => '少油少糖',
                    'name' => '少油少糖'
                ],
                [
                    'id' => '生活必备',
                    'name' => '生活必备'
                ],
            ],
            'value' => '',
        ],
        [
            'name' => 'price',
            'description' => '价格',
            'type' => 'number',
            'value' => '',
        ],
        [
            'name' => 'pic',
            'description' => '图片',
            'type' => 'image',
            'value' => '',
        ],
        [
            'name' => 'description',
            'description' => '描述',
            'type' => 'text',
            'value' => '',
        ]
    ];

    public function category()
    {
        return $this->belongsTo(Categories::class, 'categories_id')->first();
    }

    public function getCategoryNameAttribute()
    {
        $category = $this->category();
        return $category ? $category->name : "";
    }

    public function comments()
    {
        return $this->hasMany(ProductsComments::class)->get();
    }
}
