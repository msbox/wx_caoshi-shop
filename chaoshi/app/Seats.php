<?php

namespace App;

class Seats extends Entity
{
    //
    const columns = [
        'id' => 'ID',
        'seat_no' => '座位号',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    protected $fillable = [
        'seat_no'
    ];

    const createFields = self::editFields;

    const editFields = [
        [
            'name' => 'seat_no',
            'description' => '座位号',
            'type' => 'string',
            'value' => '',
        ],
    ];
}
