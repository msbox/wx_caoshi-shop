<?php

namespace App;

class Categories extends Entity
{
    //
    const columns = [
        'id' => 'ID',
        'name' => '分类名',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    protected $fillable = [
        'name'
    ];

    const createFields = self::editFields;

    const editFields = [
        [
            'name' => 'name',
            'description' => '分类名',
            'type' => 'string',
            'value' => '',
        ],
    ];

    const searchFields = [
        [
            'name' => 'name',
            'description' => '分类名称',
            'type' => 'like'
        ]
    ];

    public function products()
    {
        return $this->hasMany(Products::class);
    }
}
