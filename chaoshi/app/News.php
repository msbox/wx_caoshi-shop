<?php

namespace App;

class News extends Entity
{
    protected $fillable = [
        'title', 'content'
    ];

    const nav = '广告轮播图管理';

    public const columns = [
        'id' => 'ID',
        'title' => '标题',
        'content' => '内容'
    ];

    const searchFields = [
        [
            'name' =>'title',
            'description' => '标题',
            'type' => 'like'
        ]
    ];

    public const createFields = [
      [
          'name'=> 'title', 'description' => '标题', 'type' => 'string'
      ],
      [
          'name' => 'content', 'description' => '内容', 'type' => 'text'
      ]
    ];

    public const editFields = self::createFields;

    protected $isDelete = true;
}
