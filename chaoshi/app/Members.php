<?php

namespace App;

class Members extends Entity
{
    protected $fillable = [
        'nickname', 'real_name', 'phone_number', 'sex', 'pic', 'points', 'openid'
    ];

    const columns = [
        'id' => 'ID',
        'nickname' => '昵称',
        'real_name' => '真实姓名',
        'phone_number' => '手机号',
        'sex' => '性别',
        'pic' => [
            'name' => '头像',
            'type' => 'image',
        ],
        'points' => '积分',
        'openid' => 'OPENID',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    public function comments()
    {
        $this->hasMany(ProductsComments::class, 'members_id')->get();
    }

    public function incPoints($number) {
        $this->increment('points', $number);
    }
}
