<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // 是否删除
    protected $isDelete = true;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    const columns = [
        'id' => 'ID',
        'name' => '用户名',
        'email' => '邮箱',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    const searchFields = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const createFields = [
        [
            'name' => 'name',
            'description' => '用户名',
            'type' => 'string',
            'value' => '',
        ],
        [
            'name' => 'email',
            'description' => '邮箱',
            'type' => 'string',
            'value' => '',
        ],
        [
            'name' => 'password',
            'description' => '密码',
            'type' => 'password',
            'value' => '',
        ],
        [
            'name' => 'password_confirm',
            'description' => '确认密码',
            'type' => 'password',
            'value' => '',
        ],
    ];

    const editFields = [
        [
            'name' => 'password',
            'description' => '密码',
            'type' => 'password',
            'value' => '',
        ],
        [
            'name' => 'password_confirm',
            'description' => '确认密码',
            'type' => 'password',
            'value' => '',
        ],
    ];

    protected $actions = [];

    public function getIsDeleteAttribute()
    {
        return $this->isDelete;
    }

    public function getExtensionActionsAttribute()
    {
        return $this->actions;
    }
}
