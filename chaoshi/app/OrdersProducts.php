<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{
    //
    protected $fillable = [
        'orders_id',
        'number',
        'products_id',
        'price'
    ];
}
