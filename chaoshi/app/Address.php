<?php

namespace App;

class Address extends Entity
{

    protected $table = 'address';

    protected $fillable = [
        'userName', 'area','code','phone','members_id','address'
    ];

    const nav = '地址管理';

    /**
     * $table->string('userName'); //收件人
     * $table->string('area'); //地区
     * $table->string('address'); //详细地址
     * $table->string('code'); //邮编
     * $table->string('phone'); //手机号码
     */
    public const columns = [
        'id' => 'ID',
        'userName' => '收件人',
        'area' => '地区',
        'code' => '邮编',
        'phone' => '手机号码',
    ];

    const searchFields = [
        [
            'name' => 'userName',
            'description' => '收件人',
            'type' => 'like'
        ]
    ];

    public const createFields = [];

    public const editFields = self::createFields;

    protected $isDelete = false;
}
