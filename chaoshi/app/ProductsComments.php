<?php

namespace App;

class ProductsComments extends Entity
{
    //
    const columns = [
        'id' => 'ID',
        'product_name' => '产品名称',
        'member_nickname' => '用户昵称',
        'title' => '标题',
        'description' => '内容',
        'reply' => '回复',
        'created_at' => '创建时间',
        'updated_at' => '更新时间'
    ];

    const searchFields = [

    ];

    protected $actions = [
        'reply' => [
            'route' => 'products.comments.reply', // 地址
            'model' => "App.ProductsComments", //实体类路径
            'icon' => '回复',
            'fields' => [
                [
                    'name' => 'reply',
                    'description' => '回复',
                    'type' => 'text'
                ]
            ]
        ],
        'reply1' => [
            'route' => 'products.comments.reply', // 地址
            'model' => "App.ProductsComments", //实体类路径
            'icon' => '回复1',
            'fields' => [
                [
                    'name' => 'reply',
                    'description' => '回复',
                    'type' => 'number'
                ]
            ]
        ],
    ];

    protected $fillable = [
        'title', 'description', 'products_id', 'members_id', 'reply', 'nickname', 'product_name'
    ];

    public function product()
    {
        return $this->belongsTo(Products::class, 'products_id')->first();
    }

    public function member()
    {
        return $this->belongsTo(Members::class, 'members_id')->first();
    }

    public function getProductNameAttribute()
    {
        $product = $this->product();
        return $product ? $product->title : "";
    }

    public function getProductPicAttribute()
    {
        $product = $this->product();
        return $product ? $product->pic : "";
    }

    public function getMemberNicknameAttribute()
    {
        $member = $this->member();
        return $member ? $member->nickname : "";
    }

}
