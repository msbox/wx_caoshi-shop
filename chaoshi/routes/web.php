<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')
//    ->middleware('auth:web')
    ->namespace('Admin')
    ->group(function () {
        Route::get('/', function () {
            return view('admin.index');
        });
        Route::get('/welcome', function () {
            return view('admin.welcome');
        });

        // 商品管理
        Route::get('/products', 'ProductsController@index')->name('products');
        Route::get('/products_add', 'ProductsController@create')->name('products.add');
        Route::get('/products/{id}', 'ProductsController@show')->name('products.edit');
        Route::post('/products', 'ProductsController@store')->name('products.store');
        Route::post('/products/{id}', 'ProductsController@update')->name('products.update');
        Route::delete('/products/{id}', 'ProductsController@destroy')->name('products.delete');

        // 商品分类
        Route::get('/categories', 'CategoriesController@index')->name('categories');
        Route::get('/categories_add', 'CategoriesController@create')->name('categories.add');
        Route::post('/categories', 'CategoriesController@store')->name('categories.store');
        Route::get('/categories/{id}', 'CategoriesController@show')->name('categories.edit');
        Route::post('/categories/{id}', 'CategoriesController@update')->name('categories.update');
        Route::delete('/categories/{id}', 'CategoriesController@destroy')->name('categories.delete');

        // 商品评论
        Route::get('comments', 'CommentsController@index')->name('products.comments');
        Route::post('comments/{id}/reply', 'CommentsController@reply')->name('products.comments.reply');

        // 用户管理
        Route::get('/members', 'MembersController@index')->name('members');
        Route::post('/members', 'MembersController@store')->name('members.store');
        Route::get('/members/{id}', 'MembersController@show')->name('members.edit');
        Route::post('/members/{id}', 'MembersController@update')->name('members.update');
        Route::delete('/members/{id}', 'MembersController@destroy')->name('members.delete');

        // 座位管理
        Route::get('/news', 'NewsController@index')->name('news');
        Route::post('/news', 'NewsController@store')->name('news.store');
        Route::get('/news_add', 'NewsController@create')->name('news.add');
        Route::get('/news/{id}', 'NewsController@show')->name('news.edit');
        Route::post('/news/{id}', 'NewsController@update')->name('news.update');
        Route::delete('/news/{id}', 'NewsController@destory')->name('news.delete');

        // 地址
        Route::get('/address', 'AddressController@index')->name('address');

        // 订单管理
        Route::get('/orders', 'OrdersController@index')->name('orders');
        Route::post('/orders', 'OrdersController@store')->name('orders.store');
        Route::get('/orders/{id}', 'OrdersController@show')->name('orders.edit');
        Route::post('/orders/{id}', 'OrdersController@update')->name('orders.update');
        Route::delete('/orders/{id}', 'OrdersController@destroy')->name('orders.delete');
        Route::get('/orders_add', function () {
            return view('admin.orders.add');
        })->name('orders.add');

        //Route::post('/orders/{id}', 'OrdersController@wancheng')->name('orders.filish');

        // 管理员管理
        Route::get('/users', 'UsersController@index')->name('users');
        Route::post('/users', 'UsersController@store')->name('users.store');
        Route::get('/users_add', 'UsersController@create')->name('users.add');
        Route::get('/users/{id}', 'UsersController@show')->name('users.edit');
        Route::post('/users/{id}', 'UsersController@update')->name('users.update');
        Route::delete('/users/{id}', 'UsersController@destory')->name('users.delete');

        // 设置
        Route::get('/setting', 'ConfigsController@index')->name('configs');
        Route::post('/setting/{key}', 'ConfigsController@setValue')->name('configs.set');
        Route::post('/setting', 'ConfigsController@set')->name('configs.post');

        // 文件上传
        Route::post('/file/upload','UploadController@upload')->name('file.upload');

        // 通用操作
        Route::get('/actions/{id}', function (\Illuminate\Http\Request $request, $id) {
            $modelClass = str_replace('.', '\\', $request->input('model'));
            $key = $request->input('key');
            $model = new $modelClass();
            return view('admin.action', [
                'id' => $id,
                'fields' => $model->getExtensionFields($key),
                'route' => $request->input('route')
            ]);
        })->name('actions.open');
    });

Route::middleware('auth:web')->get('/logout', 'Auth\LoginController@logout')->name('logout');

// 登录
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.post');
